package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"sync"
	"time"
)

func do(url string, res chan string, done *sync.WaitGroup, ready *sync.WaitGroup) {

	defer done.Done()
	ready.Wait()

	res <- fmt.Sprintf("executing GET %s at %s", url, time.Now())

	resp, err := http.Get(url)
	if err != nil {
		fmt.Println(err)
	}

	if resp == nil {
		return
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("error : ", err)
	}

	res <- fmt.Sprintf("%s : %s", string(body), time.Now())
}

func main() {

	url := flag.String("url", "", "target URL")
	runs := flag.Int("runs", 1, "number of staggered runs")
	users := flag.Int("users", 1, "number of concurrent users per run")
	stagger := flag.Int("stagger", 1000, "milliseconds between runs")

	flag.Parse()

	if *url == "" {
		flag.PrintDefaults()
		os.Exit(-1)
	}

	var done sync.WaitGroup
	var ready sync.WaitGroup

	res := make(chan string)

	go func() {
		for {
			select {
			case msg := <-res:
				fmt.Println(msg)
			}
		}
	}()

	for i := 0; i < *runs; i++ {

		fmt.Println("Starting batch", i+1, "at", time.Now())
		ready.Add(1)

		for j := 0; j < *users; j++ {
			done.Add(1)
			go do(*url, res, &done, &ready)
		}

		ready.Done()

		time.Sleep(time.Millisecond * time.Duration(*stagger))
	}

	done.Wait()
}
